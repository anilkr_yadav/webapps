package com.friendship.mainapp.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by root on 9/6/17.
 */
@Controller
public class HomeController  {
    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String welcome()
    {
        return "index";
    }
}
